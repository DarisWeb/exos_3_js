let ms = 0;
let sec = 0;
let i;


function fplay() {
    i = setInterval(func, 10);
}

function func() {
    let elsec = document.getElementById("sec");
    let elms = document.getElementById("ms");
    if (ms > 99) {
        ms = 0;
        sec++;
        if (sec < 10)
            stringsec = "0" + sec;
        else
            stringsec = sec;
        elsec.innerText = stringsec;
    }
    if (ms < 10)
        stringms = "0" + ms;
    else
        stringms = ms;
    elms.innerText = stringms;
    ms++;
}

function fpause() {
    clearInterval(i);
}

function fstop() {
    let elsec = document.getElementById("sec");
    let elms = document.getElementById("ms");
    elsec.innerText = "00";
    elms.innerText = "00";
    ms = 0;
    sec = 0;
    clearInterval(i);

}